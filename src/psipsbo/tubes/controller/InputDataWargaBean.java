/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psipsbo.tubes.controller;

import psipsbo.tubes.model.InputDataWarga;
import psipsbo.tubes.ui.FormPersyaratanFrame;

/**
 *
 * @author HP
 */
public class InputDataWargaBean {
    private FormPersyaratanFrame form;
    private InputDataWarga input;
    private InputAccessDatabase dao;
    
    public InputDataWargaBean(FormPersyaratanFrame form, InputDataWarga input){
        this.form = form;
        this.input = input;
        
        dao = new InputAccessDatabase();
    }
    public void inputData(){
        dao.save(input);
    }
}
